package sv.edu.bitlab.pupusap

import android.content.Context
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import sv.edu.bitlab.pupusap.HistoryScreen.HistoryActivity
import sv.edu.bitlab.pupusap.Models.*
import sv.edu.bitlab.pupusap.Network.ApiService
import sv.edu.bitlab.pupusap.RellenoRecyclerView.RellenoViewHolder
import sv.edu.bitlab.pupusap.RellenoRecyclerView.RellenosListAdapter

class MainActivity : AppCompatActivity(),
    RellenoViewHolder.RellenoViewHolderListener,
    RellenoFragmentDialog.OnRellenoFragmentInteraction {
    var rellenos = arrayListOf<Relleno>()
    private lateinit var rellenosList:RecyclerView
    override fun onButtonClicked(relleno: String) {
        rellenos.add(Relleno(relleno, -1))
        val adapter = rellenosList.adapter as RellenosListAdapter
        adapter.notifyDataSetChanged()
    }

    //COMENTARIO PARA USAR GITFLOW
    override fun onCounterBtnClick(relleno: String, masa: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onMaizClick(relleno: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onArrozClick(relleno: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    val orden = TakenOrden()

    val pupusaStringResources = hashMapOf(
        QUESO to R.string.pupusa_queso,
        FRIJOLES to R.string.frijol_con_queso,
        REVUELTAS to R.string.revueltas
    )

    var botonesMaiz = hashMapOf<String, Button>()
    var botonesArroz = hashMapOf<String, Button>()
    var quesoIzquierda: Button? = null
    var frijolIzquierda: Button? = null
    var revueltaIzquierda: Button? = null

    var quesoDerecha: Button? = null
    var frijolDerecha: Button? = null
    var revueltasDerecha: Button? = null
    var loadingContainer: View? = null

    var sendButton: Button? = null

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val orden = TakenOrden()
        val loadingContainer = findViewById<View>(R.id.loadingContainer)
        rellenosList = findViewById<RecyclerView>(R.id.rellenosList)
        rellenosList.layoutManager = LinearLayoutManager(this)
        rellenosList.adapter = RellenosListAdapter(
            arrayListOf<Relleno>(),
            listener = this
        )
        loadingContainer.visibility = View.VISIBLE

        ApiService.create().getRellenos().enqueue(object : Callback<List<Relleno>> {
            override fun onFailure(call: Call<List<Relleno>>, t: Throwable) {
                loadingContainer.visibility = View.GONE
                AlertDialog.Builder(getContent())
                    .setTitle("ERROR")
                    .setMessage("Error con el servidor lo sentimos")
                    .setNeutralButton("ok", null)
                    .create()
                    .show()
            }

            override fun onResponse(
                call: Call<List<Relleno>>,
                response: Response<List<Relleno>>
            ) {
                loadingContainer.visibility = View.GONE
                rellenos = response.body()!! as ArrayList<Relleno>
                val adapter = rellenosList.adapter as RellenosListAdapter
                adapter.rellenos = rellenos
                adapter.notifyDataSetChanged()
            }
        })
        setSupportActionBar(findViewById(R.id.mainToolbar))
    }


    fun getContent(): Context {
        return this
    }




    fun displayCounters() {
        for ((key,value) in orden.maiz){
            val resource = pupusaStringResources[key]
            val text = this.resources.getString(resource!!, value)
            botonesMaiz[key]!!.text = text
        }


        for ((key,value) in orden.arroz){
            val resource = pupusaStringResources[key]
            val text = this.resources.getString(resource!!, value)
            botonesArroz[key]!!.text = text
        }

    }

    fun addMaiz(relleno: String) {
        orden.maiz[relleno] = orden.maiz[relleno]!! + 1
        val contador = orden.maiz[relleno]
        val resource = pupusaStringResources[relleno]
        val text = this.resources.getString(resource!!, contador)
        botonesMaiz[relleno]!!.text = text
    }
    fun addArroz(relleno: String) {
        orden.arroz[relleno] = orden.arroz[relleno]!! + 1
        val contador =  orden.arroz[relleno]
        val resource = pupusaStringResources[relleno]
        val text = this.resources.getString(resource!!, contador)
        botonesArroz[relleno]!!.text = text
    }

    private fun confirmarOrden() {
        val intent = Intent(this, DetalleOrdeActivity::class.java)
        intent.putExtra(ORDEN,orden)
        this.startActivity(intent)
    }

    fun showLoading(show: Boolean) {
        val visibility = if(show) View.VISIBLE else View.GONE
        loadingContainer!!.visibility = visibility
    } // region Menu Setup

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.historial -> {
                val intent = Intent(this, HistoryActivity::class.java)
                startActivity(intent)
            }
            R.id.new_relleno -> {
               val rellenoDialog = RellenoFragmentDialog.newInstance()
                rellenoDialog.show(supportFragmentManager, "RELLENO_FRAGMENT")
            }
            R.id.precio_unidad -> {

            }
        }
        return super.onOptionsItemSelected(item)
    }





    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)

    }

    companion object{
        const val MAIZ = "MAIZ"
        const val ARROZ = "ARROZ"
    }

}
