package sv.edu.bitlab.pupusap.RellenoRecyclerView

import android.view.View
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import sv.edu.bitlab.pupusap.Models.Relleno
import sv.edu.bitlab.pupusap.R

class RellenoViewHolder(itemView: View, val listener: RellenoViewHolderListener): RecyclerView.ViewHolder(itemView) {
  private lateinit var maizBtn:Button
  private lateinit var arrozBtn:Button

  fun bindData(relleno: Relleno) {
    maizBtn = itemView.findViewById(R.id.maizBtn)
    arrozBtn = itemView.findViewById(R.id.arrozBtn)
    maizBtn.text = relleno.nombre
    arrozBtn.text = relleno.nombre
    maizBtn.setOnClickListener { listener.onMaizClick(relleno.nombre) }
    arrozBtn.setOnClickListener { listener.onArrozClick(relleno.nombre) }
  }

  interface RellenoViewHolderListener{
    fun onCounterBtnClick(relleno: String, masa: String)

    fun onMaizClick(relleno: String)
    fun onArrozClick(relleno: String)
  }
}